import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import 'StartGame.dart';

void main() {
  runApp(const ColumnExample());
}

class ColumnExample extends StatelessWidget {
  const ColumnExample({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Math Game',
        theme: ThemeData(
          primarySwatch: Colors.teal,
        ),
        home: const Scaffold(
          body: SafeArea(
            child: StartGame(),
          ),
        ),
      ),
    );
  }
}
