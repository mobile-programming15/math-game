import 'package:flutter/material.dart';
import 'HomePage.dart';

class StartGame extends StatelessWidget {
  const StartGame({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: const BoxConstraints(maxWidth: 300.0),
        child: Column(
          children: <Widget>[
            const Spacer(flex: 5),
            Container(
                width: 570,
                height: 570,
                child: Image.network("https://media.discordapp.net/attachments/988538114693808200/1083599258676117504/Screenshot_2023-03-10_105451-removebg-preview.png?width=507&height=662")
            ),
            const Spacer(flex: 3),
            OutlinedButton(
                style: OutlinedButton.styleFrom(
                  fixedSize: Size(200, 60),
                  backgroundColor: Colors.teal[300],
                ),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return HomePage();
                  }));
                },
                child: const Text(
                  'START GAME!!',
                  style: TextStyle(fontSize: 25, color: Colors.white),
                )),
            const Spacer(flex: 20),
          ],
        ),
      ),
    );
  }
}
