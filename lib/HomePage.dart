import 'dart:math';
import 'package:flutter/material.dart';
import 'package:math_game/button.dart';
import 'package:math_game/showmessage.dart';
import 'StartGame.dart';
import 'const.dart';
import 'dart:async';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late Timer _timer;
  int _currentTime = 30; // เวลาที่กำหนดไว้เริ่มต้นคือ 30 วินาที

  @override
  void initState() {
    super.initState();
    _startTimer();
  }

  void _startTimer() {
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        if (_currentTime > 0) {
          _currentTime--;
        } else {
          _timer.cancel();
          _backStart();
          _endGame();

        }
      });
    });
  }

  void _endGame() {
    // แสดงคะแนนที่ได้รับ
    showDialog(
        context: context,
        builder: (context) {
          return ShowMessage(
              message: "Game Over !! Your score : $score",
              onTap: () => Navigator.of(context).pop(HomePage()),
              icon: Icons.close);
        });
  }

  void _backStart() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const StartGame()),
    );
  }

  //numpad list
  List<String> numPad = [
    '7',
    '8',
    '9',
    'C',
    '4',
    '5',
    '6',
    'DEL',
    '1',
    '2',
    '3',
    '=',
    '0',
  ];

  //number A,number B
  int numberA = 5;
  int numberB = 1;
  //user answer
  String userAnswer = '';

  //score
  int score = 0;

  //user tapped
  void buttonTapped(String button) {
    setState(() {
      if (button == '=') {
        // check correct or incorrect
        checkResult();
      } else if (button == 'C') {
        // clear  input
        userAnswer = '';
      } else if (button == 'DEL') {
        // delete number
        if (userAnswer.isNotEmpty) {
          userAnswer = userAnswer.substring(0, userAnswer.length - 1);
        }
      } else if (userAnswer.length < 3) {
        // maximum 3
        userAnswer += button;
      }
    });
  }

  //check user correct or not
  void checkResult() {
    if (numberA + numberB == int.parse(userAnswer)) {
      setState(() {
        score += 1;
      });

      showDialog(
          context: context,
          builder: (context) {
            return ShowMessage(
              message: 'Correct! Score: $score',
              onTap: goNextQuestion,
              icon: Icons.arrow_forward,
            );
          });
    } else {
      showDialog(
          context: context,
          builder: (context) {
            return ShowMessage(
              message: 'try again',
              onTap: goBackQuestion,
              icon: Icons.rotate_left,
            );
          });
    }
  }

  //create random numbers
  var randomNum = Random();

  void goNextQuestion() {
    //dismiss alert dialog
    Navigator.of(context).pop();
    //reset values
    setState(() {
      userAnswer = '';
    });
    //create a new question
    numberA = randomNum.nextInt(10);
    numberB = randomNum.nextInt(10);
  }

  void goBackQuestion() {
    //dismiss alert dialog
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[300],
      body: Column(
        children: [
          //level
          Container(
            height: 160,
            color: Colors.blueAccent,
            child: Center(
              child: Text(
                'Time left: $_currentTime',
                style: TextStyle(
                  fontSize: 30,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),

          //question
          Expanded(
            child: Container(
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    //question
                    Text(
                      numberA.toString() + ' + ' + numberB.toString() + ' = ',
                      style: whiteTextStyle,
                    ),

                    //answer box

                    Container(
                      height: 50,
                      width: 100,
                      decoration: BoxDecoration(
                        color: Colors.blueAccent[400],
                        borderRadius: BorderRadius.circular(4),
                      ),
                      child: Center(
                        child: Text(
                          userAnswer,
                          style: whiteTextStyle,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),

          //number pad
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.all(4.0),
              child: GridView.builder(
                itemCount: numPad.length,
                physics: const NeverScrollableScrollPhysics(),
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 4,
                ),
                itemBuilder: (context, index) {
                  return MyButton(
                    child: numPad[index],
                    onTap: () => buttonTapped(numPad[index]),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}