import 'package:flutter/material.dart';
import 'const.dart';

class ShowMessage extends StatelessWidget {
  final String message;
  final VoidCallback onTap;
  final icon;

  const ShowMessage({
    Key? key,
    required this.message,
    required this.onTap,
    required this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.blue[300],
      content: Container(
        height: 200,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
// the result
            Text(
              message,
              style: whiteTextStyle,
            ),

//button next question
            GestureDetector(
              onTap: onTap,
              child: Container(
                padding: EdgeInsets.all(4),
                decoration: BoxDecoration(
                  color: Colors.blueAccent,
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Icon(
                  icon,
                  color: Colors.white,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

